package testsentence;

import com.appframework.model.sentence.Sentence;
import com.appframework.model.sentence.SentenceUtils;
import com.appframework.model.text.Text;
import com.appframework.model.text.TextUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Arrays;


public class TestApp {
    private static Logger LOG = LogManager.getLogger(TestApp.class);

    @Test()
    public void TestSentenceUtils() {
        SentenceUtils.parseWords("Hello my dear, friend", SentenceUtils.DEFAULT_REGEX_FOR_SENTENCE);
    }

    @Test
    public void TestSentenceClass() {
        Sentence testSentence = new Sentence("Today i`ll learn java String`");
        LOG.info(testSentence);
    }

    @Test
    public void TestTextUtilsParsing() {
        Text text = new Text(TextUtils.DEFAULT_FILE_PATH);
        LOG.info(text);
        for(int k=0; k<30;k++){
            LOG.info("["+text.getNextSentence()+"]");
        }

    }
    @Test
    public void SentenceSimilarValueTest(){
        Sentence testsentence = new Sentence("hello hello & 8654fdg hello!");
        LOG.info(SentenceUtils.calculateSimilarWordsValue(testsentence));
        LOG.info(SentenceUtils.getSentenceWordSet(testsentence));
    }
    @Test
    public void getSentenceWordSetTest(){
        LOG.info(SentenceUtils.getSentenceWordSet(new Sentence("Hello hello hello")));
    }
}