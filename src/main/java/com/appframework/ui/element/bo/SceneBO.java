package com.appframework.ui.element.bo;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.layout.VBox;

import static com.appframework.ui.utils.Settings.*;

public class SceneBO {
    private Scene scene;
    private VBox box;

    public SceneBO() {
        box = new VBox(DEFAULT_BUTTON_SPACING);
        box.setStyle(CURRENT_BACKGROUND_STYLE);
        box.setAlignment(Pos.CENTER);
        scene = new Scene(box, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public Scene getScene() {
        return scene;
    }
    public VBox getBox(){return box;}

    public void addAllElements(Control... elements) {
        box.getChildren().addAll(elements);
    }
}
