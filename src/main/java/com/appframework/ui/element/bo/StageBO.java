package com.appframework.ui.element.bo;

import javafx.stage.Stage;

public class StageBO {
    private Stage window;

    public StageBO(Stage setter) {
        window = setter;
    }

    public StageBO setTitle(String title) {
        window.setTitle(title);
        return this;
    }

    public StageBO setScene(SceneBO scene) {
        window.setScene(scene.getScene());
        return this;
    }

    public void show() {
        window.show();
    }

}
