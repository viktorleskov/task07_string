package com.appframework.ui.element;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.appframework.ui.utils.Settings.CURRENT_BUTTON_STYLE;
import static com.appframework.ui.utils.Settings.CURRENT_TEXT_STYLE;

public abstract class ElementFabric {
    private static ResourceBundle bundle;

    public static void setLanguage(String language) {
        Locale locale = new Locale(language);
        bundle = ResourceBundle.getBundle("menu", locale);
    }

    public static Button getExitButton() {
        Button exitButton = new Button(bundle.getString("Exit"));
        exitButton.setStyle(CURRENT_BUTTON_STYLE);
        exitButton.setOnAction(e -> System.exit(0));
        return exitButton;
    }

    public static Label getLabel(String key) {
        Label label = new Label(bundle.getString(key));
        label.setStyle(CURRENT_TEXT_STYLE);
        return label;
    }

    public static Label getTextLabel(String text) {
        Label label = new Label(text);
        label.setStyle(CURRENT_TEXT_STYLE);
        return label;
    }

    public static Button getCustomButton(EventHandler<ActionEvent> value, String key) {
        Button button = new Button(bundle.getString(key));
        button.setStyle(CURRENT_BUTTON_STYLE);
        button.setOnAction(value);
        return button;
    }

    public static ChoiceBox<? extends java.io.Serializable> getChoiceBox(EventHandler<ActionEvent> value, List<String> variants) {
        ChoiceBox<java.io.Serializable> box = new ChoiceBox<>();
        box.getItems().addAll(variants);
        box.setOnAction(value);
        box.setStyle(CURRENT_TEXT_STYLE);
        return box;
    }
}
