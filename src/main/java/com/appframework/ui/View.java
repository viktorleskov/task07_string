package com.appframework.ui;

import com.appframework.controller.MainControllers;
import com.appframework.ui.element.ElementFabric;
import com.appframework.ui.element.bo.SceneBO;
import com.appframework.ui.element.bo.StageBO;
import com.appframework.ui.utils.Language;
import com.appframework.ui.utils.Settings;
import com.appframework.ui.utils.TaskReader;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

import static com.appframework.ui.utils.Settings.APPLICATION_DEFAULT_LANGUAGE;

public class View extends Application {
    private static Logger LOG = LogManager.getLogger(View.class);
    private StageBO mainWindow;
    private SceneBO mainMenuScene;
    private SceneBO taskScene;
    private SceneBO settingsScene;
    private SceneBO languageSelectionScene;
    private Locale locale;
    private ResourceBundle bundle;

    @Override
    public void start(Stage primaryStage) {
        LOG.info("Application starting...");
        mainWindow = new StageBO(primaryStage);
        this
                .setApplicationLanguage(APPLICATION_DEFAULT_LANGUAGE)
                .updateUserInterface()
                .showWindow();
    }

    private View updateUserInterface() {
        LOG.info("Updating GUI...");
        this
                .initializeAllScene()
                .initializeSceneElements();
        mainWindow
                .setTitle(bundle.getString("Title"))
                .setScene(mainMenuScene);
        return this;
    }

    private View initializeAllScene() {
        LOG.info("Initializing scenes...");
        mainMenuScene = new SceneBO();
        taskScene = new SceneBO();
        settingsScene = new SceneBO();
        languageSelectionScene = new SceneBO();
        return this;
    }

    private void initializeSceneElements() {
        LOG.info("Initializing scenes elements...");
        mainMenuScene.addAllElements(
                ElementFabric.getLabel("MainLabel"),
                ElementFabric.getCustomButton(event -> changeScene(taskScene), "Task"),
                ElementFabric.getCustomButton(event -> changeScene(settingsScene), "SettingsLabel"),
                ElementFabric.getExitButton()
        );
        taskScene.addAllElements(
                ElementFabric.getLabel("TaskLabel"),
                ElementFabric.getLabel("EnterTaskLabel"),
                ElementFabric.getChoiceBox(event -> MainControllers.changeTask(taskScene, 2), TaskReader.readTaskList(locale)),
                ElementFabric.getCustomButton(event -> changeScene(mainMenuScene), "Back")
        );
        settingsScene.addAllElements(
                ElementFabric.getLabel("SettingsLabel"),
                ElementFabric.getCustomButton(event -> changeScene(languageSelectionScene), "LanguageLabel"),
                ElementFabric.getCustomButton(event -> changeApplicationTheme(), "ChangeTheme"),
                ElementFabric.getCustomButton(event -> changeScene(mainMenuScene), "Back")
        );
        languageSelectionScene.addAllElements(
                ElementFabric.getLabel("LanguageLabel"),
                ElementFabric.getCustomButton(event -> setApplicationLanguage(Language.UK), "Ukr"),
                ElementFabric.getCustomButton(event -> setApplicationLanguage(Language.EN), "Eng"),
                ElementFabric.getCustomButton(event -> setApplicationLanguage(Language.PL), "Pl"),
                ElementFabric.getCustomButton(event -> setApplicationLanguage(Language.DE), "De"),
                ElementFabric.getCustomButton(event -> setApplicationLanguage(Language.RU), "Ru"),
                ElementFabric.getCustomButton(event -> changeScene(settingsScene), "Back")
        );
    }

    private void initializeBundle(Language language) {
        ElementFabric.setLanguage(language.getLanguage());
        locale = new Locale(language.getLanguage());
        bundle = ResourceBundle.getBundle("menu", locale);
    }

    private View setApplicationLanguage(Language language) {
        LOG.info("Setting up application language.");
        initializeBundle(language);
        if (mainMenuScene != null) updateUserInterface();
        return this;
    }

    private void changeApplicationTheme() {
        LOG.info("Application theme was changed.");
        Settings.changeTheme();
        if (mainMenuScene != null) updateUserInterface();
    }

    private void changeScene(SceneBO scene) {
        mainWindow.setScene(scene);
    }

    private void showWindow() {
        mainWindow.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
