package com.appframework.ui.utils;

public enum Language {
    EN("en"), UK("uk"), PL("pl"), DE("de"), RU("ru");

    Language(String lang) {
        this.language = lang;
    }

    private String language;

    public String getLanguage() {
        return language;
    }
}
