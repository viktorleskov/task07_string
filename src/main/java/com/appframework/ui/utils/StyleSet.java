package com.appframework.ui.utils;

public enum StyleSet {

    DEFAULT_BACKGROUND_STYLE("-fx-background-color: BEIGE;"),
    BLACK_THEME_BACKGROUND_STYLE("-fx-background-color: Black;"),

    DEFAULT_BUTTON_STYLE("-fx-background-color: darkslateblue; -fx-text-fill: white;"),
    BLACK_THEME_BUTTON_STYLE("-fx-background-color: DarkViolet; -fx-text-fill: Azure;"),

    DEFAULT_TEXT_STYLE("-fx-font: normal bold 20px 'serif' "),
    BLACK_THEME_TEXT_STYLE("-fx-font: normal bold 20px 'serif'; -fx-text-fill: Azure; ");

    private String style;

    StyleSet(String style) {
        this.style = style;
    }

    public String getStyle() {
        return style;
    }
}
