package com.appframework.ui.utils;

public final class Settings {
    private Settings() {
    }

    final public static int DEFAULT_BUTTON_SPACING = 15;
    public static int DEFAULT_WIDTH = 900;
    public static int DEFAULT_HEIGHT = 720;
    public static Language APPLICATION_DEFAULT_LANGUAGE = Language.EN;
    //----Task constant
    static int MAX_TASK_VALUE = 16;
    //----UI settings constants
    public static String CURRENT_BACKGROUND_STYLE = StyleSet.DEFAULT_BACKGROUND_STYLE.getStyle();
    public static String CURRENT_BUTTON_STYLE = StyleSet.DEFAULT_BUTTON_STYLE.getStyle();
    public static String CURRENT_TEXT_STYLE = StyleSet.DEFAULT_TEXT_STYLE.getStyle();

    public static void changeTheme() {
        if (CURRENT_BACKGROUND_STYLE.equals(StyleSet.DEFAULT_BACKGROUND_STYLE.getStyle())) {
            CURRENT_BACKGROUND_STYLE = StyleSet.BLACK_THEME_BACKGROUND_STYLE.getStyle();
            CURRENT_BUTTON_STYLE = StyleSet.BLACK_THEME_BUTTON_STYLE.getStyle();
            CURRENT_TEXT_STYLE = StyleSet.BLACK_THEME_TEXT_STYLE.getStyle();
        } else {
            CURRENT_BACKGROUND_STYLE = StyleSet.DEFAULT_BACKGROUND_STYLE.getStyle();
            CURRENT_BUTTON_STYLE = StyleSet.DEFAULT_BUTTON_STYLE.getStyle();
            CURRENT_TEXT_STYLE = StyleSet.DEFAULT_TEXT_STYLE.getStyle();
        }
    }
}
