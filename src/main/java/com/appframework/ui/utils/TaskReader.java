package com.appframework.ui.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.appframework.ui.utils.Settings.MAX_TASK_VALUE;

public final class TaskReader {

    public static List<String> readTaskList(Locale lang) {
        Locale locale = new Locale(lang.getLanguage());
        ResourceBundle bundle = ResourceBundle.getBundle("menu", locale);
        List<String> list = new LinkedList<>();
        for (int i = 1; i <= MAX_TASK_VALUE; i++) {
            list.add(bundle.getString(Integer.toString(i)));
        }
        return list;
    }
}
