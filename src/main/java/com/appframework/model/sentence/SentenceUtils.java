package com.appframework.model.sentence;

import com.appframework.model.word.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class SentenceUtils {
    public static final String DEFAULT_REGEX_FOR_SENTENCE = "[\\w’`+]+";
    private static Pattern pattern;
    private static Matcher matcher;

    private SentenceUtils() {
    }

    public static List<Word> parseWords(String sentence, String regex) {
        List<Word> words = new ArrayList<>();
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(sentence);
        while (matcher.find()) {
            words.add(new Word(sentence.substring(matcher.start(), matcher.end())));
        }
        return words;
    }

    public static int calculateSimilarWordsValue(Sentence sentence) {
        Sentence wordSet = new Sentence(getSentenceWordSet(sentence));
        int similarityValue = 0;
        for (int k = 0; k < wordSet.getValueOfWords(); k++) {
            similarityValue += calculateWordOccurrencesNumber(wordSet.getWordByIndex(k), sentence);
        }
        return similarityValue;
    }

    public static String getSentenceWordSet(Sentence sentence) {
        return sentence.getWords()
                .stream()
                .distinct()
                .collect(Collectors.toList()).toString();
    }

    private static int calculateWordOccurrencesNumber(Word word, Sentence sentence) {
        return (int) sentence.getWords()
                .stream()
                .filter(word::equals)
                .count();
    }

}
