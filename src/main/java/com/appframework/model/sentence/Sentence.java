package com.appframework.model.sentence;

import com.appframework.model.word.Word;

import java.util.List;

public class Sentence {
    private List<Word> words;

    public Sentence(String sentence) {
        this.words = SentenceUtils.parseWords(sentence, SentenceUtils.DEFAULT_REGEX_FOR_SENTENCE);
    }

    @Override
    public String toString() {
        StringBuilder bufString = new StringBuilder();
        for (Word word : words) {
            bufString.append(word.toString()).append(" ");
        }
        return bufString.toString();
    }

    public Word getWordByIndex(int index) {
        return words.get(index);
    }

    public List<Word> getWords(){
        return words;
    }

    public int getValueOfWords() {
        return words.size();
    }
}
