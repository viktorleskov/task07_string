package com.appframework.model.text;

import com.appframework.model.sentence.Sentence;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class TextUtils {
    public static final String DEFAULT_FILE_PATH = "D:\\EducationEPAM\\TA_LAB_RD\\task07_String\\src\\main\\resources\\text.txt";
    public static final String DEFAULT_REGEX_FOR_FILE = "[^.]+";
    private static Pattern pattern;
    private static Matcher matcher;

    private TextUtils() {
    }

    public static List<Sentence> parseSentence(String fileName, String regex) {
        List<Sentence> sentences = new ArrayList<>();
        String stringFile = fileToString(fileName);
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(stringFile);
        while (matcher.find()) {
            sentences.add(new Sentence(stringFile.substring(matcher.start(), matcher.end())));
        }
        return sentences;
    }

    private static String fileToString(String fileName) {
        StringBuilder stringText = new StringBuilder();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
            stringText.append(br.lines().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringText.toString();
    }

    public static String processingTextBeforeOutput(String text) {
        StringBuilder builder = new StringBuilder(text);
        StringBuilder bufStringBuilder = new StringBuilder();
        for (int k = 0, j = 0; k < text.length(); k++, j++) {
            if (text.charAt(k) == '\n') {
                j = 0;
            }
            if (j >= 80) {
                bufStringBuilder = new StringBuilder(builder.substring(0, k) + '\n' + builder.substring(k, builder.length()));
                builder = new StringBuilder(bufStringBuilder);
                j = 0;
            }
        }
        return bufStringBuilder.toString();
    }
}
