package com.appframework.model.text;

import com.appframework.model.sentence.Sentence;

import java.util.List;

public class Text {
    private List<Sentence> sentences;
    private int sentenceIndexValue;

    public Text(String fileName) {
        sentences = TextUtils.parseSentence(fileName, TextUtils.DEFAULT_REGEX_FOR_FILE);
    }

    public Sentence getNextSentence() {
        if (sentenceIndexValue == sentences.size()) {
            setSentenceIndexToStart();
        }
        return sentences.get(sentenceIndexValue++);
    }

    public Sentence getSentenceByIndex(int index) {
        return sentences.get(index);
    }

    public List<Sentence> getSentences(){
        return sentences;
    }

    public void setSentenceIndexToStart() {
        sentenceIndexValue = 0;
    }

    public int getValueOfSentences() {
        return sentences.size();
    }

    @Override
    public String toString() {
        StringBuilder bufString = new StringBuilder();
        for (Sentence sens : sentences) {
            bufString.append(sens).append("\n");
        }
        return bufString.toString();
    }
}
