package com.appframework.model.word;

import com.appframework.model.symbol.Symbol;

import java.util.ArrayList;
import java.util.List;

public class Word {
    private List<Symbol> symbols;

    public Word(String word) {
        this.symbols = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            this.symbols.add(new Symbol(word.charAt(i)));
        }
    }

    public String getSymbols() {
        return symbols.toString();
    }

    @Override
    public String toString() {
        StringBuilder bufString = new StringBuilder();
        for (Symbol symb : symbols) {
            bufString.append(symb.getCharacter());
        }
        return bufString.toString();
    }

    @Override
    public int hashCode() {
        return getSymbols().length()+toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode()==obj.hashCode();
    }
}
