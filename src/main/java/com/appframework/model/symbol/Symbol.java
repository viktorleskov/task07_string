package com.appframework.model.symbol;

public class Symbol {
    private Character character;

    public Symbol(char symbol) {
        this.character = new Character(symbol);
    }

    public Character getCharacter() {
        return character;
    }
}
