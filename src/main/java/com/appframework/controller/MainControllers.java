package com.appframework.controller;

import com.appframework.ui.element.bo.SceneBO;
import com.taskcases.TaskCases;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.appframework.model.text.TextUtils.processingTextBeforeOutput;

public class MainControllers {
    private static Logger LOG = LogManager.getLogger(MainControllers.class);
    private static Map<Integer, Executable> methodsTask = new LinkedHashMap<>();
    private static Label toChange;
    static{
        methodsTask.put(1,  TaskCases::taskCase1);
        methodsTask.put(2,  TaskCases::taskCase2);
        methodsTask.put(3,  TaskCases::taskCase3);
        methodsTask.put(4,  TaskCases::taskCase4);
        methodsTask.put(5,  TaskCases::taskCase5);
        methodsTask.put(6,  TaskCases::taskCase6);
        methodsTask.put(7,  TaskCases::taskCase7);
        methodsTask.put(8,  TaskCases::taskCase8);
        methodsTask.put(9,  TaskCases::taskCase9);
        methodsTask.put(10, TaskCases::taskCase10);
        methodsTask.put(11, TaskCases::taskCase11);
        methodsTask.put(12, TaskCases::taskCase12);
        methodsTask.put(13, TaskCases::taskCase13);
        methodsTask.put(14, TaskCases::taskCase14);
        methodsTask.put(15, TaskCases::taskCase15);
        methodsTask.put(16, TaskCases::taskCase16);
    }
    private MainControllers(){}
    public static void changeTask(SceneBO taskScene,int boxIndex){
        setLabelForTasks((Label)taskScene.getBox().getChildren().get(1));
        String pattern = "[0-9]+";
        String text = ((ChoiceBox)taskScene.getBox().getChildren().get(boxIndex)).getValue().toString();
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        while(m.find()){
            executeTask(methodsTask.get(Integer.parseInt(text.substring(m.start(),m.end()))));
        }
    }
    private static void executeTask(Executable task){
        LOG.info("processing task..");
        task.execute();
    }
    public static void outputResult(String result){
        toChange.setText(processingTextBeforeOutput(result));
    }
    private static void setLabelForTasks(Label toChange){
        MainControllers.toChange = toChange;
    }
}
