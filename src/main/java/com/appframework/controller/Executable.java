package com.appframework.controller;

import javafx.scene.control.Label;

@FunctionalInterface
public interface Executable {
    public void execute();
}
