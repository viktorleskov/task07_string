package com.taskcases;


import com.appframework.model.sentence.Sentence;
import com.appframework.model.sentence.SentenceUtils;
import com.appframework.model.text.Text;
import com.appframework.model.text.TextUtils;
import com.appframework.model.word.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static com.appframework.controller.MainControllers.outputResult;

public final class TaskCases {
    private static Logger LOG = LogManager.getLogger(TaskCases.class);
    private static Text text;

    /**
     * 1.Знайти найбільшу кількість речень тексту, в яких є однакові слова.
     */
    public static final void taskCase1() {
        LOG.info("TaskCase 1 running");
        text = new Text(TextUtils.DEFAULT_FILE_PATH);
        Map<Integer, Sentence> similarityRating = new HashMap<>();
        for (int k = 0; k < text.getValueOfSentences(); k++) {
            int similarityValue = SentenceUtils.calculateSimilarWordsValue(text.getSentenceByIndex(k));
            if (similarityValue > 0) {
                similarityRating.put(similarityValue, text.getSentenceByIndex(k));
            }
        }
        outputResult(getSortedMapToString(similarityRating));
    }

    /**
     * 2.Вивести всі речення заданого тексту у порядку зростання кількості слів у кожному з них.
     */
    public static final void taskCase2() {
        LOG.info("TaskCase 2 running");
        text = new Text(TextUtils.DEFAULT_FILE_PATH);
        Map<Integer, Sentence> valueOfWordsRating = new HashMap<>();
        for (int k = 0; k < text.getValueOfSentences(); k++) {
            if (text.getSentenceByIndex(k).getValueOfWords() > 0) {
                valueOfWordsRating.put(text.getSentenceByIndex(k).getValueOfWords(), text.getSentenceByIndex(k));
            }
        }
        outputResult(getSortedMapToString(valueOfWordsRating));
    }

    /**
     * 3.Знайти таке слово у першому реченні, якого немає ні в одному з інших речень.
     */
    public static final void taskCase3() {
        LOG.info("TaskCase 3 running");
        text = new Text(TextUtils.DEFAULT_FILE_PATH);
        List<Word> firstSentenceWords = SentenceUtils.parseWords
                (text.getSentenceByIndex(0).toString(), SentenceUtils.DEFAULT_REGEX_FOR_SENTENCE);
        for (int k = 0; k < firstSentenceWords.size(); k++) {
            if (!checkTextForWord(text, firstSentenceWords.get(k), 0)) {
                outputResult(firstSentenceWords.get(k).toString());
            }
        }
        outputResult("None found");
    }

    /**
     * 4.У всіх запитальних реченнях тексту знайти і надрукувати без повторів слова заданої довжини.
     */
    public static final void taskCase4() {
        LOG.info("TaskCase 4 running");
        outputResult("taskCase4!");
    }

    /**
     * 5.У кожному реченні тексту поміняти місцями перше слово, що починається на голосну букву з найдовшим словом.
     */
    public static final void taskCase5() {
        LOG.info("TaskCase 5 running");
        outputResult("taskCase5!");
    }

    /**
     * 6.Надрукувати слова тексту в алфавітному порядку по першій букві.
     * Слова, що починаються з нової букви, друкувати з абзацного відступу.
     */
    public static final void taskCase6() {
        LOG.info("TaskCase 6 running");
        outputResult("taskCase6!");
    }

    /**
     * 7.Відсортувати слова тексту за зростанням відсотку голосних букв
     * (співвідношення кількості голосних до загальної кількості букв у слові).
     */
    public static final void taskCase7() {
        LOG.info("TaskCase 7 running");
        outputResult("taskCase7!");
    }

    /**
     * 8.Слова тексту, що починаються з голосних букв,
     * відсортувати в алфавітному порядку по першій приголосній букві слова.
     */
    public static final void taskCase8() {
        LOG.info("TaskCase 8 running");
        outputResult("taskCase8!");
    }

    /**
     * 9.Всі слова тексту відсортувати за зростанням кількості заданої букви у слові.
     * Слова з однаковою кількістю розмістити у алфавітному порядку.
     */
    public static final void taskCase9() {
        LOG.info("TaskCase 9 running");
        outputResult("taskCase9!");
    }

    /**
     * 10.Є текст і список слів. Для кожного слова з заданого списку знайти,
     * скільки разів воно зустрічається у кожному реченні,
     * і відсортувати слова за спаданням загальної кількості входжень.
     */
    public static final void taskCase10() {
        LOG.info("TaskCase 10 running");
        outputResult("taskCase10!");
    }

    /**
     * 11.У кожному реченні тексту видалити підрядок максимальної довжини,
     * що починається і закінчується заданими символами.
     */
    public static final void taskCase11() {
        LOG.info("TaskCase 11 running");
        outputResult("taskCase11!");
    }

    /**
     * 11.У кожному реченні тексту видалити підрядок максимальної довжини,
     * що починається і закінчується заданими символами.
     */
    public static final void taskCase12() {
        LOG.info("TaskCase 12 running");
        outputResult("taskCase12!");
    }

    /**
     * 13.Відсортувати слова у тексті за спаданням кількості входжень заданого символу,
     * а у випадку рівності – за алфавітом.
     */
    public static final void taskCase13() {
        LOG.info("TaskCase 13 running");
        outputResult("taskCase13!");
    }

    /**
     * 14.У заданому тексті знайти підрядок максимальної довжини, який є паліндромом, тобто,
     * читається зліва на право і справа на ліво однаково.
     */
    public static final void taskCase14() {
        LOG.info("TaskCase 14 running");
        outputResult("taskCase14!");
    }

    /**
     * 15.Перетворити кожне слово у тексті, видаливши з нього всі наступні
     * (попередні) входження першої (останньої) букви цього слова.
     */
    public static final void taskCase15() {
        LOG.info("TaskCase 15 running");
        outputResult("taskCase15!");
    }

    /**
     * 16.У певному реченні тексту слова заданої довжини замінити вказаним підрядком,
     * довжина якого може не співпадати з довжиною слова.
     */
    public static final void taskCase16() {
        LOG.info("TaskCase 16 running");
        outputResult("taskCase16!");
    }

    private static String getSortedMapToString(Map<Integer, Sentence> map) {
        LinkedHashMap<Integer, Sentence> sortedMap = new LinkedHashMap<>();
        map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
        return sortedMap.toString();
    }

    private static boolean checkTextForWord(Text text, Word word, int indexOfSentenceContainingWord) {
        List<Sentence> sentences = new ArrayList<>();
        for (int k = indexOfSentenceContainingWord + 1; k < text.getValueOfSentences(); k++) {
            sentences.add(text.getSentenceByIndex(k));
        }
        for (Sentence sens : sentences) {
            if (checkSentenceForWord(sens, word)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkSentenceForWord(Sentence sentence, Word word) {
        List<Word> words = new ArrayList<>();
        for (int k = 0; k < sentence.getValueOfWords(); k++) {
            words.add(sentence.getWordByIndex(k));
        }
        for (Word checker : words) {
            if (checker.equals(word)) {
                return true;
            }
        }
        return false;
    }
}
